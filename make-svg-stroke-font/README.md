# Commands to include svg's into a font

1. Convert the font to ttx using the commands below
2. Run the `insert-svgs.py` script to insert the svg's 
3. Convert the ttx back to ttf using the commands below.

## Conversion to and from ttx

Conversion between ttf ttx is done using [fonttools](https://github.com/fonttools/fonttools), a python based library for modifying fonts.

To convert from ttf to ttx run:
```
ttx name-of-the-font.ttf
```

To convert from ttx to ttf run:
```
ttx name-of-the-font.ttx
```

## Utilities
- `insert-svgs.py` adds an SVG table to the given font based on a folder with SVG files.

### insert-svgs.py

Inserts a collection of SVG's into the SVG table of a font. If the font doesn't have an SVG table yet it will be added. The SVG's must have the same glyphName (as encoded in the font) as the glyph you want it to replace. `A.svg` → `A`, `zero.svg` → `zero`.

The script is called with three arguments, the filename of the input ttx, the path to the folder containing the SVG's, the output ttx. 

```
insert-svgs.py fontname-in.ttx foldernameofsvgs fontname-out.ttx
```
