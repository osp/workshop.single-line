import lxml.etree as et
import glob
import os.path

def make_glyphlist ():
  with open('glyphlist.txt', 'r') as h:
    mapping = []
    name_to_unicode = {}
    unicode_to_name = {}

    for line in h.readlines():
      if not line.startswith('#'):
        name, code = line.split(';')
        if not ' ' in code:
          code = int(code, base=16)
        else:
          code = tuple(map(lambda c: int(c, base=16), code.split(' ')))

        name_to_unicode[name] = code
        unicode_to_name[code] = name
        mapping.append((name, code))

  return mapping, name_to_unicode, unicode_to_name


def lookupGlyphID (ttx, glyphName):
  glyphID = ttx.find('GlyphOrder/GlyphID[@name="{}"]'.format(glyphName))
  if glyphID is not None:
    return int(glyphID.get('id'))
  else:
    return None

"""
  Finds svg table. If it's not there make one.
"""
def getSVGTable (ttx):
  svgTable = ttx.find('SVG')
  if svgTable is None:
    svgTable = et.SubElement(ttx.getroot(), 'SVG')

  return svgTable

"""
  Insert given svg into the ttx
  ttx: ElementTree
  glyphID: integer
  svg: string
"""
def insertSVG (ttx, glyphID, svgPath):
  svgTable = getSVGTable(ttx)
  svgDoc = svgTable.find('svgDoc[@startGlyphID="{}"]'.format(glyphID))
  if svgDoc is None:
    svgDoc = et.SubElement(svgTable, 'svgDoc', {
      'endGlyphID': str(glyphID),
      'startGlyphID': str(glyphID) 
    })

  # Insert SVG Code
  with open(svgPath, 'r') as svgIn:
    svg = et.parse(svgIn, et.XMLParser(remove_blank_text=True))
    svg.getroot().set('id', 'glyph{}'.format(glyphID, parser=et.XMLParser(remove_blank_text=True)))
    svg.getroot().set('viewBox', '0 1000 1000 1000')
    svgDoc.text = et.CDATA(et.tostring(svg, pretty_print=True))

if __name__ == '__main__':
  import sys

  if len(sys.argv) < 3:
    print("Usage: make-gvar-table.py in.ttx out.ttx")

  pathIn = sys.argv[1]
  svgFolder = sys.argv[2]
  pathOut = sys.argv[3] if len(sys.argv) > 3 else sys.argv[1]

  _, __, unicode_name_map = make_glyphlist()

  with open(pathIn, 'r') as h:
    ttx = et.parse(h, et.XMLParser(remove_blank_text=True))
    svgs = []
    for svgPath in glob.glob(os.path.join(svgFolder, '*.svg')):
      codepoint, _ = os.path.splitext(os.path.basename(svgPath))
      codepoint = int(codepoint)
      if codepoint in unicode_name_map:
        glyphName = unicode_name_map[codepoint]
        print('Found SVG {} for {}'.format(svgPath, glyphName))
        glyphID = lookupGlyphID(ttx, glyphName)
        if not glyphID:
          print('Could not find GlyphID, probably not part of the font. Skipping.')
        else:
          svgs.append((glyphID, svgPath))
      
    svgs = sorted(svgs, key=lambda s: s[0])

    for glyphID, svgPath in svgs:
        insertSVG(ttx, glyphID, svgPath)
    
    ttx.write(pathOut, pretty_print=True, doctype='<?xml version="1.0" encoding="UTF-8"?>')