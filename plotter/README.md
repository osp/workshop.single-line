# Plot a metapost font

This is a recipe to make a letterproof from a metapost file. The script generates 

0. Make sure you've cd'ed into this folder.
1. Copy over the latest mp-file from the version you want to plot to this folder
2. Adjust the copied metapost file to hide the grid, hints and dots (line 21, 22, 23) set all of them to `0`, and adjust the output template (line 9) to `"to-plot/%c.svg"`.
3. Generate the svg's using metapost `mpost -interaction=batchmode glyphs.mp`
4. Combine the individual glyphs with the combine script: `python3 ../svg-generator/combine.py to-plot combined.svg`
5. Convert the generated SVG to hpgl with the `svg2hpgl.py`-script, run it in python 2! (or use inkscape): `python2 svg2hpgl.py combined.svg > to-plot.hpgl`
6. Plot the generated hpgl with chiplotle: `plot_hpgl_file.py to-plot.hpgl`

# Requirements:

## Python 2

- lxml
- chiplotle

To install:

```
pip2 install lxml chiplotle
```

## Python 3

- lxml

```
pip3 install lxml
```
