from hpgl_output_adapted import HpglOutputAdapted
from sys import argv

e = HpglOutputAdapted()
e.affect([
    '--orientation', '90',
    '--force', '0',
    '--overcut', '0',
    '--precut', 'false',
    '--flat', '4',
    '--center', 'true',
    '--toolOffset', '0',
    '--autoAlign', 'false',
    '--resolutionX', '2100',
    '--resolutionY', '2100',
    '--speed', '{}'.format(10),
    '--penCount', '{}'.format(1),
    '--force', '{}'.format(3),
    argv[1]], False)

print(e.hpgl)