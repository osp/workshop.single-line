var testing, inReload, inText, inScale, iframe, group, versions, pad_link;

function get (url, callback) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = callback;
	xhttp.open("GET", url, true);
	xhttp.send();
}

function list_chars (version) {
	get("projects/" + version + "/chars.txt", function () {
		var paths = this.response.split('\n');
		return paths;
	});
}

function nav_versions() { 
	get("index.txt", function() {
		if (this.readyState == 4 && this.status == 200) {
			var index = this.response.split('\n')
			index.forEach(function(item, i){
				var name = item.split('-')
				name = name[name.length-1]
				versions.innerHTML += '<option data-href="'+item+'" value="'+name+'">'+name+'</option>'
			});
            
			versions.onchange = function (e) {
				group = this.value;
				loadLetters(document.querySelector('#inText').value)
				location.hash = group
				var selected = this.options[this.selectedIndex]
				console.log(selected)
				pad_link.innerHTML = 'open pad -> '+ selected.value 
				pad_link.href = selected.getAttribute('data-href') 

			};

			group = versions.value;

			let selected = versions.options[versions.selectedIndex];
			pad_link.innerHTML = 'open pad -> '+ selected.value; 
			pad_link.href = selected.getAttribute('data-href');
		}
	});
}


function scale(value){
	console.log(testing.style)
	testing.style.transform = 'scale(' + value + ')'
}


function rand(){
	return Math.floor((Math.random() * 10000) + 1)
}


function loadLetters(letters){
	var tabLetters = letters.split('')
	testing.innerHTML = ''
	tabLetters.forEach(function(item, i){
		var itemCode = item.charCodeAt(0)
		var ra = rand()
		testing.innerHTML += '<img src="projects/' + group + '/svg/' + itemCode + '.svg?rand=' + ra + '" />' 
	})
}


document.addEventListener("DOMContentLoaded", (event) => {
  group = location.hash.replace('#', '')
	testing = document.querySelector('#testing')
	pad_link = document.getElementById('pad_link')
	inReload = document.querySelector('#inReload')
	inText = document.querySelector('#inText')
	inScale = document.querySelector('#inScale')
	versions = document.querySelector('#versions')
	inReload.addEventListener('click', (event) => {
		loadLetters(inText.value)
	})

	inScale.addEventListener('change', (event) => {
		scale(inScale.value)
	})

	inText.addEventListener('change', () => {
		loadLetters(inText.value);
	});

	nav_versions();
})
