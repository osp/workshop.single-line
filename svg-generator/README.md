## SVG GENERATOR 

`python scale.py width=<valeur> height=<valeur> 'MOT'` 

## Combine.py

The combine script combines multiple svg files into one. 

```
python combine.py folder-with-svgs outputfile [rowheight] [outputwidth]
```

The script is called with two positional arguments, and two subsequent optional arguments.

The first argument is a path to the folder with svgs to include.

The second argument is the path of the outputfile.

The third argument, optional, sets the height of the svgs in the file, files are scaled to fit. **default: 200**

The fourth argument, optional, sets the intended width of the generated documet. **default: 1000**